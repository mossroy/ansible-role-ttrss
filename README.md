# Ansible role Tiny Tiny RSS

Simple Ansible role to install and configure Tiny Tiny RSS (tt-rss) in my self-hosting context

The variables `ttrss_postgresql_password` and `ttrss_url` need to be set.
The active server must be in a `ttrss_active` group.